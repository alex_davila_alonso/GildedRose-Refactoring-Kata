﻿using System.Collections.Generic;
using GildedRoseKata;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GildedRoseTests;

[TestClass]
public class GildedRoseTest
{
    // This is the test unit test, just simple unit test
    [TestMethod]
    public void Foo()
    {
        var items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        Assert.AreEqual(items[0].Name, "foo");
    }

    [TestMethod]
    public void TestSellInValue()
    {
        var items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
        var app = new GildedRose(items);
        Assert.AreEqual(app.Items[0].SellIn, 0);
    }

    [TestMethod]
    public void TestQuality()
    {
        var items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
        var app = new GildedRose(items);
        Assert.AreEqual(app.Items[0].Quality, 0);
    }

    [TestMethod]
    public void TestUpdateQuality()
    {
        var items = new List<Item> { new Item { Name = "foo", SellIn = 1, Quality = 1 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].SellIn, 0);
        Assert.AreEqual(app.Items[0].Quality, 0);
    }

    [TestMethod]
    public void TestAfterSellinTwiceRate()
    {
        var items = new List<Item> { new Item { Name = "foo", SellIn = -1, Quality = 2 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 0);
    }

    [TestMethod]
    public void TestQualityNeverBelow0()
    {
        var items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        app.UpdateQuality();
        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 0);
    }

    [TestMethod]
    public void TestAgedBrieQualityUpBy2()
    {
        var items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 10, Quality = 0 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 1);
    }

    [TestMethod]
    public void TestQualityNeverAbove50()
    {
        var items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 10, Quality = 50 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        app.UpdateQuality();
        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 50);
    }

    [TestMethod]
    public void TestSulfurasNeverSoldOrDecreasesQuality()
    {
        var items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        app.UpdateQuality();
        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].SellIn, 0);
        Assert.AreEqual(app.Items[0].Quality, 80);
    }

    [TestMethod]
    public void TestBackstagePasses()
    {
        var items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 13, Quality = 0 } };
        var app = new GildedRose(items);

        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 1);
        app.UpdateQuality();
        app.UpdateQuality();

        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 5);
        app.UpdateQuality();
        app.UpdateQuality();
        app.UpdateQuality();
        app.UpdateQuality();

        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 16);
        app.UpdateQuality();
        app.UpdateQuality();
        app.UpdateQuality();
        app.UpdateQuality();

        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 0);
    }

    [TestMethod]
    public void TestConjuredNormal()
    {
        var items = new List<Item> { new Item { Name = "Conjured", SellIn = 10, Quality = 10 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 8);
    }

    [TestMethod]
    public void TestConjudredAfter7Days()
    {
        var items = new List<Item> { new Item { Name = "Conjured", SellIn = 0, Quality = 10 } };
        var app = new GildedRose(items);
        app.UpdateQuality();
        Assert.AreEqual(app.Items[0].Quality, 6);
    }
}