﻿// See https://aka.ms/new-console-template for more information

using GildedRoseKata;

public class Program
{
    public static void Main(string[] args)
    {
        Item item1 = new Item();
        item1.Name = "Aged Brie";
        item1.SellIn = 7;
        item1.Quality = 0;

        Item item2 = new Item();
        item2.Name = "Rose";
        item2.SellIn = 7;
        item2.Quality = 20;

        Item item3 = new Item();
        item3.Name

        List<Item> newList = new List<Item>();
        newList.Add(item1);
        newList.Add(item2);

        GildedRose gildedRose = new GildedRose(newList);

        for (int i = 0; i < 30; i++)
        {
            Console.WriteLine($"Day {i + 1}");
            Console.WriteLine(gildedRose);
            gildedRose.UpdateQuality();
        }
    }
}
